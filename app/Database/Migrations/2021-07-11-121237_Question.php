<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Question extends Migration
{
	public function up()
	{
		//
		$this->db->enableForeignKeyChecks();
		$this->forge->addField([
			'id_question int(255) NOT NULL primary key AUTO_INCREMENT',
			'context_id int(255)',
			'question varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL',
		  'reverse enum("y","n") CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL',
		  'created_at datetime(0) NULL DEFAULT NULL',
		  'updated_at datetime(0) NULL DEFAULT NULL',
		  'deleted_at datetime(0) NULL DEFAULT NULL'
		]);
		// Membuat primary key
		// $this->forge->addKey('id_question', TRUE);
		// $this->forge->addForeignKey('context_id', 'context', 'id_context');

		// Membuat tabel
		$this->forge->createTable('question', TRUE);
	}

	public function down()
	{
		//
		$this->forge->dropTable('question');
	}
}
