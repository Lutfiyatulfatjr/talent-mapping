<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Context extends Migration
{
	public function up()
	{
		//
		$this->forge->addField([
			'id_context int(255) NOT NULL primary key AUTO_INCREMENT',
			'context varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL'
		]);

		// Membuat primary key
		$this->forge->addKey('id_context', TRUE);

		// Membuat tabel news
		$this->forge->createTable('context', TRUE);
	}

	public function down()
	{
		//
		$this->forge->dropTable('context');
	}
}
