<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Choice extends Migration
{
	public function up()
	{
		//
		$this->forge->addField([
			'id_choice int(255) NOT NULL primary key AUTO_INCREMENT',
			'choice varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL',
			'value int(255) NULL DEFAULT NULL',
		  'reverse enum("y","n") CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL',
		  'created_at datetime(0) NULL DEFAULT NULL',
		  'updated_at datetime(0) NULL DEFAULT NULL',
		  'deleted_at datetime(0) NULL DEFAULT NULL'
		]);
		// Membuat primary key
		$this->forge->addKey('id_choice', TRUE);

		// Membuat tabel
		$this->forge->createTable('choice', TRUE);
	}

	public function down()
	{
		//
		$this->forge->dropTable('choice');
	}
}
