<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Context extends Seeder
{
	public function run()
	{
		// membuat data
		$news_data = [
			[
				'context' => 'Past Work'
			],
			[
				'context' => 'Present Work'
			],
			[
				'context' => 'Future Work'
			],
			[
				'context' => 'Personality Things'
			],
			[
				'context' => 'General'
			],
			[
				'context' => 'Unfavorable'
			],
			[
				'context' => 'Negative Aspect'
			]

		];

		foreach($news_data as $data){
			// insert semua data ke tabel
			$this->db->table('context')->insert($data);
		}
	}
}
