<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Question extends Seeder
{
	public function run()
	{
		//
		$question = [
			[
					'context_id'=>'1',
					'question'=>'Saya senang jika orang lain menghargai hasil kerja saya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>'',
			],
			[
					'context_id'=>'1',
					'question'=>'Saya merasa apa yang telah saya lakukan akan mendefinisikan saya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya sering melakukan manuver untuk tetap menyelesaikan pekerjaan saya meskipun berbeda dengan rencana awal.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya percaya jika tiap pekerjaan akan memiliki pola tertentu.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Orang lain kerap memuji bagaimana saya tetap bisa mengatur pekerjaan di saat sulit.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya seringkali menolak pekerjaan yang tidak sesuai dengan value saya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya sering mengambil keputusan beresiko.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Seringkali saya dipuji dengan keahlian presentasi saya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'saya senang membandingkan hasil pekerjaan saya dengan orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya memiliki kemampuan untuk mencari keterkaitan antar satu pekerjaan dengan pekerjaan lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Sebelum memutuskan sesuatu, saya mempertimbangkan pro dan kontra.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saat bekerja, saya merasakan kepuasan saat membantu orang lain merasakan keberhasilan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Sebelum mengerjakan suatu pekerjaan, saya akan mencari tahu deadline dan struktur kerjanya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Ketika keluar dari pekerjaan, seringkali yang saya rindukan adalah berinteraksi dengan orang di dalamnya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saat mendapatkan pekerjaan, saya mencari tahu tujuan akhir pekerjaan tersebut terlebih dahulu.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya sering dipuji dalam bekerja karena kemampuan untuk memproyeksikan sesuatu.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saat bekerja, saya tidak merasa berkeberatan jika harus menyesuaikan dengan orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Jika bekerja di dalam kelompok, saya berupaya agar semua orang terlibat.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Dalam bekerja dalam tim, saya melihat keunikan dari masing-masing anggota tim.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Ketika selesai pekerjaan, saya akan mengevaluasi pekerjaan tersebut.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya senang mempelajari hal-hal baru.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Memaksimalkan potensi untuk menyelesaikan pekerjaan adalah salah satu hal penting bagi saya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya senang melakukan reward kecil ketika saya mampu menyelesaikan pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Pekerjaan yang baik menurut saya adalah pekerjaan yang mampu membuat diri kita membangun banyak relasi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya selalu menyelesaikan pekerjaan yang saya mulai.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'saya menikmati pencarian solusi dari sebuah permasalahan di tempat kerja.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya senang ketika dianggap mampu dalam melaksanakan suatu pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Saya memikirkan cara terbaik untuk menyelesaikan suatu pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'1',
					'question'=>'Pekerjaan yang saya anggap menyenangkan adalah ketika berkaitan dengan banyak orang.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menyukai jika saya memiliki kesibukan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Biasanya, saya adalah orang terlebih dahulu memulai pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya mampu mengubah rencana pada detik terakhir.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menyukai pekerjaan yang berkaitan dengan data.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menikmati mengatur hal-hal terkait pekerjaan saya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya percaya jika suatu pekerjaan haruslah dapat membuat kita merasa bermanfaat.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya percaya jika suatu pekerjaan haruslah dapat membuat kita merasa bermanfaat.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya tidak merasa terganggu saat berseberangan pilihan dengan orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya tidak merasa terganggu saat berseberangan pilihan dengan orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya senang menjelaskan perihal apa yang sedang saya kerjakan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya senang menjelaskan perihal apa yang sedang saya kerjakan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menyukai iklim kompetisi saat bekerja.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menyukai iklim kompetisi saat bekerja.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Setiap pekerjaan memiliki keterkaitan dengan pekerjaan lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Setiap pekerjaan memiliki keterkaitan dengan pekerjaan lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya memahami pekerjaan dengan melihat bagaimana pekerjaan itu dilakukan sebelumnya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya memahami pekerjaan dengan melihat bagaimana pekerjaan itu dilakukan sebelumnya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menikmati rutinitas yang dapat diprediksi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menikmati rutinitas yang dapat diprediksi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menyukai pekerjaan yang melibatkan orang lain secara emosional.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menyukai pekerjaan yang melibatkan orang lain secara emosional.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menggunakan tujuan akhir pekerjaan sebagai panduan kerja.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya menggunakan tujuan akhir pekerjaan sebagai panduan kerja.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saat bekerja, saya sering memperkirakan akan menjadi seperti apa pekerjaan ini nantinya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saat bekerja, saya sering memperkirakan akan menjadi seperti apa pekerjaan ini nantinya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saat bekerja, saya senang menegutarakan ide-ide baru.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saat bekerja, saya senang menegutarakan ide-ide baru.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya merasa, bekerja akan lebih menyenangkan jika melibatkan semua orang di dalam tim.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya merasa, bekerja akan lebih menyenangkan jika melibatkan semua orang di dalam tim.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Ketika bekerja, saya akan mengumpulkan semua informasi berkaitan tentang pekerjaan tersebut.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Ketika bekerja, saya akan mengumpulkan semua informasi berkaitan tentang pekerjaan tersebut.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya senang pekerjaan yang membutuhkan pemilkiran mendalam.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya senang pekerjaan yang membutuhkan pemilkiran mendalam.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saat bekerja, saya mencari kesempatan untuk mempelajari hal yang sebelumnya tidak saya ketahui.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saat bekerja, saya mencari kesempatan untuk mempelajari hal yang sebelumnya tidak saya ketahui.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya memaksimalkan apa yang saya pahami untuk menyelesaikan pekerjaan sebaik-baiknya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya memaksimalkan apa yang saya pahami untuk menyelesaikan pekerjaan sebaik-baiknya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Setiap pekerjaan pasti memiliki cara untuk diselesaikan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Setiap pekerjaan pasti memiliki cara untuk diselesaikan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya tidak pernah berhenti di tengah pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya jarang merasa terintimidasi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya tidak pernah berhenti di tengah pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya ingin merasa menjadi bagian penting dalam satu pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya jarang merasa terintimidasi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Dalam bekerja, tidak hanya kerja keras yang dibutuhkan, tetapi juga kerja cerdas.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Saya ingin merasa menjadi bagian penting dalam satu pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Dalam pekerjaan, saya senang bertemu dengan orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Dalam bekerja, tidak hanya kerja keras yang dibutuhkan, tetapi juga kerja cerdas.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'2',
					'question'=>'Dalam pekerjaan, saya senang bertemu dengan orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya menikmati pekerjaan yang menantang.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya selalu bersemangat untuk melakukan pekerjaan selanjutnya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya ingin memahami keterkaitan satu fakta dengan fakta yang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya terbiasa untuk menyusun jadwal untuk pekerjaan saya nantinya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saat mencari pekerjaan, saya memprioritaskan pekerjaan yang sesuai dengan nilai yang saya anut.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saat melamar pekerjaan, saya melihat bagaimana keterkaitan pekerjaan tersebut nantinya dengan yang sebelumnya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya melakukan evaluasi terhadap pekerjaan yang telah dilakukan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Ketika bekerja, saya seringkali diminta bantuan untuk menyelesaikan friksi antar teman kerja.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saat akan mengerjakan tugas, saya memikirkan ide mengenai bagaimana pengerjaan yang lebih baik.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Setiap orang dalam kelompok pasti memiliki keistimewaan yang dapat membantu meraih tujuan kelompok.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Semakin beragam orang di dalam sebuah tim, maka semakin tim tersebut memliliki potensi untuk maju.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya menikmati proses berpikir.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya senang mencari pekerjaan yang sekiranya memberikan saya pembelajaran baru.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saat berkenalan dengan orang baru, saya selalu menanyakan apakah mereka mengenal seseorang yang saya kenal sebelumnya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Ketika memutuskan untuk berhenti bekerja, saya memberitahukan kepada atasan mengenai alasannya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya tidak mempermasalahkan pekerjaan baru karena saya yakin dengan kemampuan saya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'3',
					'question'=>'Saya senang mencari informasi mengenai tempat kerja yang membutuhkan banyak interaksi satu sama lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Orang di sekitar saya melihat saya sebagai seseorang yang terburu-buru.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Seringkali saya disebut sebagai orang yang konvensional.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya seringkali dianggap sebagai orang yang mengintimidasi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya sering dikatakan memiliki kemampuan untuk melihat potensi orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya sering dikatakan mampu memahami orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya sering dianggap sebagai seseorang yang pintar dan kreatif.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya sering dianggap sebagai orang yang memiliki informasi terkini dalam organisasi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya senang berpikir.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya senang memuji orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya menikmati waktu bersama keluarga.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Saya pernah dianggap "menyelamatkan" suatu organisasi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'4',
					'question'=>'Seringkali orang memandang saya sebagai orang yang senang bergaul.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Hari ini harus lebih baik dari hari kemarin.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Tindakan lebih baik daripada perencanaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Fleksibilitas dibutuhkan untuk menghadapi ketidakpastian.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Perencanaan mutlak dilakukan sebelum mengerjakan apapun.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Memegang teguh nilai adalah hal yang paling penting dalam hidup.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Kebenaran yang pahit lebih baik daripada kebohongan manis.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Karma memang nyata adanya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Keseimbangan diperlukan dalam hidup.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Setiap orang memiliki potensi mereka masing-masing.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Setiap hal dapat diprediksi jika kita memiliki informasi yang lengkap mengenainya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Manusia dapat dipahami melalui emosi mereka.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Pekerjaan yang baik dimulai dari tujuan yang jelas.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Hidup harus berada dalam harmoni dengan orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Manusia dilihat dari ide-ide yang mereka utarakan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Setiap orang harus merasa menjadi bagian dari kelompok.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Setiap individu adalah pribadi yang unik.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Setiap informasi dapat menjadi masukan penting nantinya.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Manusia adalah makhluk pembelajar.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Setiap orang memiliki kesempatan untuk sukses jika mampu memaksimalkan potensi diri.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Persahabatan adalah salah satu hal penting dalam pekerjaan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Setiap manusia bertanggungjawab atas perbuatannya masing-masing.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Permasalahan ada untuk diselesaikan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'5',
					'question'=>'Setiap orang di dunia adalah rekan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'6',
					'question'=>'Saya tidak nyaman jika berada dalam posisi harus memimpin orang lain.',
					'reverse'=>'y',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'6',
					'question'=>'Terlalu banyak informasi membuat saya bingung.',
					'reverse'=>'y',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'6',
					'question'=>'Setiap progress kecil harus diapresiasi.',
					'reverse'=>'y',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'6',
					'question'=>'Terkadang saya malu untuk berbicara dengan orang lain karena takut kehabisan bahan bicara.',
					'reverse'=>'y',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Terkadang saya dianggap sebagai seseorang yang terlalu ambisius.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Seringkali saya bertindak tanpa berpikir.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Saya sering dianggap tidak detail dalam merencanakan sesuatu.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Saya terlalu banyak memikirkan rencana sebelum bertindak.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Saya dianggap tidak fleksibel dalam hidup.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Orang memandang saya sebagai seseorang yang banyak bicara.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Saya sering dianggap sebagai orang yang tak mau kalah.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Saya sering dianggap sebagai orang yang penuh perhitungan.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Saya sering dianggap terlalu mencampuir urusan orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Saya sering dianggap sebagai seseorang yang kaku dengan jadwal dan struktur.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Seringkali, saya dianggap sebagai.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Seringkali saya dikatakan hanya fokus pada hasil.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Seringkali saya ditegur karena membayangkan hal yang belum tentu terjadi.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Seringkali saya dikatakan mengumpulkan hal-hal atau informasi yang tidak penting.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Seringkali orang lainj memprotes jika saya terlalu banyak berpikir.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Sering kali orang berpikir saya terlalu perfeksionis.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Sering kali orang berpikir jika saya tidak tulus dalam memuji mereka.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Seringkali orang berpikir saya tidak profesional dalam bekerjq.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Oranglain kadang berpikir saya terlalu mencampuri urusan mereka.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Seringkali saya dikatakan terlalu percaya diri.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Saya tidak suka jika diremehkan oleh orang lain.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			],
			[
					'context_id'=>'7',
					'question'=>'Orang mengatakan jika saya terlalu banyak bicara.',
					'reverse'=>'n',
					'created_at'=>'',
					'updated_at'=>'',
					'deleted_at'=>''
			]
		];
		foreach($question as $data){
			// insert semua data ke tabel
			$this->db->table('question')->insert($data);
		}
	}
}
