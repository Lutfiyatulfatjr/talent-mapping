<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Choice extends Seeder
{
	public function run()
	{
		//
		$choice = [
			[
            'choice'=>'Sangat Tidak  Sesuai',
            'value'=>'1',
            'reverse'=>'n',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Tidak Sesuai',
            'value'=>'2',
            'reverse'=>'n',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Kurang Sesuai',
            'value'=>'3',
            'reverse'=>'n',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Agak Sesuai',
            'value'=>'4',
            'reverse'=>'n',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Sesuai',
            'value'=>'5',
            'reverse'=>'n',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Sangat Sesuai',
            'value'=>'6',
            'reverse'=>'n',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Sangat Tidak  Sesuai',
            'value'=>'6',
            'reverse'=>'y',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Tidak Sesuai',
            'value'=>'5',
            'reverse'=>'y',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Kurang Sesuai',
            'value'=>'4',
            'reverse'=>'y',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Agak Sesuai',
            'value'=>'3',
            'reverse'=>'y',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Sesuai',
            'value'=>'2',
            'reverse'=>'y',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ],
        [
            'choice'=>'Sangat Sesuai',
            'value'=>'1',
            'reverse'=>'y',
            'created_at'=>'',
            'updated_at'=>'',
            'deleted_at'=>''
        ]
		];
		foreach($choice as $data){
			// insert semua data ke tabel
			$this->db->table('choice')->insert($data);
		}
	}
}
