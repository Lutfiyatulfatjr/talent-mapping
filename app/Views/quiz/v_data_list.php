    <button class="btn btn-primary btn-round" id="take_quiz_btn">
      <i class="material-icons">add</i> Ambil Quiz
    </button>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title ">Quiz</h4>
          <p class="card-category"></p>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th>No</th>
                <th>Tanggal</th>
                <th>Hasil</th>
                <th>Action</th>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>7 Juli 2021</td>
                  <td>........</td>
                  <td><a><span class="material-icons">visibility</span></a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
<script >
  $(document).ready(function() {
    });
  var site = "<?= base_url() ?>";
  var modul = "<?= $menu ?>";
  $( "#take_quiz_btn" ).click(function() {
    var image_load = "<div class='spinner'><img class='spinner-img' src='"+site+"/images/spinner.gif' /></div>";
    // Swal.fire($(document.filter.elements).serialize());
    $("#ajax-konten").html(image_load);
    $.get(site +'/'+ modul +'/take_quiz', {}, function (data) {
            $("#ajax-konten").html(data);
        });
  });
</script>
