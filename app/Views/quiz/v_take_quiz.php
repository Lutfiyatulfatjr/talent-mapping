    <button class="btn btn-primary btn-round" id="back_btn">
      <i class="material-icons">arrow_back</i> Kembali
    </button>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title ">Quiz</h4>
          <p class="card-category"></p>
        </div>
        <div class="card-body" id="content-items">
          <h4 class="card-title">Pilih jawaban yang paling sesuai menurutmu</h4>
          <!-- <p class="card-text">With supporting text below as a natural lead-in to additional content.</p> -->
          <a class="btn btn-primary" id="start_quiz">Mulai Quiz</a>
        </div>
      </div>
    </div>
    <script >
      $(document).ready(function() {
        });
      var site = "<?= base_url() ?>";
      var modul = "<?= $menu ?>";
      $( "#back_btn" ).click(function() {
        var image_load = "<div class='spinner'><img class='spinner-img' src='"+site+"/images/spinner.gif' /></div>";
        // Swal.fire($(document.filter.elements).serialize());
        $("#ajax-konten").html(image_load);
        $.get(site +'/'+ modul+'/data_list', {}, function (data) {
                $("#ajax-konten").html(data);
            });
      });
      $( "#start_quiz" ).click(function() {
        var image_load = "<div class='spinner'><img class='spinner-img' src='"+site+"/images/spinner.gif' /></div>";
        // Swal.fire($(document.filter.elements).serialize());
        $("#content-items").html(image_load);
        $.get(site +'/'+ modul+'/start_quiz', {}, function (data) {
                $("#content-items").html(data);
            });
      });
    </script>
