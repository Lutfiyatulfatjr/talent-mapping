<?= $this->extend('partials/template'); ?>
<?= $this->section('content'); ?>
<div class="container-fluid">
  <div class="row"  id="ajax-konten">

  </div>

</div>
<script >
  $(document).ready(function() {
    get_items();
    });
  var site = "<?= base_url() ?>";
  var modul = "<?= $menu ?>";
  function get_items(){
    var image_load = "<div class='spinner'><img class='spinner-img' src='"+site+"/images/spinner.gif' /></div>";
    // Swal.fire($(document.filter.elements).serialize());
    $("#ajax-konten").html(image_load);
    $.get(site +'/'+ modul +'/data_list', {}, function (data) {
            $("#ajax-konten").html(data);
        });
  };
</script>
<?= $this->endSection() ?>
