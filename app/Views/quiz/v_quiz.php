<link href="<?php echo base_url(); ?>/assets/css/wizard.css" rel="stylesheet" />
<div class="text-center p-0 mt-3 mb-2" >
    <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
        <!-- <h2><strong>Pilih jawaban yang menurutmu sesuai</strong></h2> -->
        <p>Pilih jawaban yang paling sesuai menurutmu</p>
        <div class="row">
            <div class="col-md-12 mx-0">
                <form id="quiz" name="quiz">
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" id="step1"></li>
                        <li id="step2"></li>
                        <li id="step3"></li>
                        <li id="step4"></li>
                        <li id="step5"></li>
                        <li id="step6"></li>
                    </ul> <!-- fieldsets -->
                    <?php for($i=0; $i<sizeof($chunks_question); $i++){?>
                    <fieldset class="overflow-auto">
                      <?php foreach($chunks_question[$i] as $question): ?>
                        <div class="form-card">
                          <div class="form-group">
                            <label for="question"><?= $question->question ?></label>
                            <?= form_hidden('question_id[]',$question->id_question) ?>
                          </div>
                          <?php foreach($data_choice as $choice): ?>
                            <?php  if( ($question->reverse == 'n' && $choice->reverse == 'n') or ($question->reverse == 'y' && $choice->reverse == 'y') ){ ?>
                                <div class="form-check form-check-radio form-check-inline">
                                  <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="<?= 'choice_'.$question->id_question; ?>" value="<?= $choice->id_choice ?>"> <?= $choice->choice ?>
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                  </label>
                                </div>
                              <?php } ?>
                            <?php endforeach; ?>
                        </div>
                      <?php endforeach; ?>
                      <?php
                        if($i==0){
                          echo '<input type="button" name="next" class="next action-button" value="Next Step" />';
                        }else if($i>0 and $i<sizeof($chunks_question)-1){
                          echo '<input type="button" name="previous" class="previous action-button-previous" value="Previous" />';
                          echo '<input type="button" name="next" class="next action-button" value="Next Step" />';
                        }else{
                          echo '<input type="button" name="previous" class="previous action-button-previous" value="Previous" />';
                          echo '<input type="button" name="submit" id="submit" class="submit action-button" value="Selesai" />';
                        }
                        ?>
                         <!-- <input type="button" name="next" class="next action-button" value="Next Step" /> -->
                    </fieldset>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>
<script >
var site = "<?= base_url() ?>";
var modul = "<?= $menu ?>";
    $(document).ready(function(){
      var current_fs, next_fs, previous_fs; //fieldsets
      var opacity;

      $(".next").click(function(){

      current_fs = $(this).parent();
      next_fs = $(this).parent().next();

      //Add Class Active
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({opacity: 0}, {
      step: function(now) {
      // for making fielset appear animation
      opacity = 1 - now;

      current_fs.css({
      'display': 'none',
      'position': 'relative'
      });
      next_fs.css({'opacity': opacity});
      },
      duration: 600
      });
      });

      $(".previous").click(function(){

      current_fs = $(this).parent();
      previous_fs = $(this).parent().prev();

      //Remove class active
      $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

      //show the previous fieldset
      previous_fs.show();

      //hide the current fieldset with style
      current_fs.animate({opacity: 0}, {
      step: function(now) {
      // for making fielset appear animation
      opacity = 1 - now;

      current_fs.css({
      'display': 'none',
      'position': 'relative'
      });
      previous_fs.css({'opacity': opacity});
      },
      duration: 600
      });
      });

      $('.radio-group .radio').click(function(){
      $(this).parent().find('.radio').removeClass('selected');
      $(this).addClass('selected');
      });

      $('#submit').off('click').on('click',function(e){
            e.preventDefault();
            var formData = new FormData($('#quiz')[0]);
            $('form').find(' :radio:checked').each(function () {
                formData.append(this.name, $(this).val());
            });
            Swal.fire({
              title: 'Do you want to submit the quiz?',
              showCancelButton: true,
              confirmButtonText: `Submit`,
            }).then((result) => {
            	/* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                  $.ajax({
                      type: "POST",
                      url  : site + '/'+modul +"/simpan_quiz",
                      data: formData,
                      contentType: false,
                      processData: false,
                      dataType: "JSON",
                      success: function (data) {
                        // console.log(data);
                          if (data.status == 'sukses') {
                            Swal.fire(
                              'Berhasil',
                              'Quiz berhasil disimpan',
                              'success'
                            )
                            window.location = "<?= base_url('quiz'); ?>";
                            // Swal.fire($(document.filter.elements).serialize());
                            // $.get(site +'/'+ modul +'/data_list', {}, function (data) {
                            //         $("#ajax-konten").html(data);
                            //     });
                          } else {
                            Swal.fire(
                              'Gagal Simpan',
                              'Quiz gagal disimpan',
                              'error'
                            )
                          }
                      },
                  });
                }
            });
            return false;
        });



    });
    </script>
