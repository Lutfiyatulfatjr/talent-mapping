<?php

namespace App\Controllers;
use App\Models\QuestionModel;
use App\Models\ChoiceModel;

class Quiz extends BaseController
{
  public function __construct()
  {
      // parent::__construct();
      $this->question = new QuestionModel();
      $this->choice = new ChoiceModel();
      helper(["form"]);
  }
    public function index()
    {
        $data['menu'] = 'quiz';
        return view('quiz/v_index',$data);
    }

    function data_list(){
      $data['menu'] = 'quiz';
      return view('quiz/v_data_list',$data);
    }

    public function take_quiz()
    {
        $data['menu'] = 'quiz';
        return view('quiz/v_take_quiz',$data);
    }
    public function start_quiz()
    {
        $data_question = $this->question->findAll();
        shuffle($data_question);
        $chunks_question = array_chunk($data_question,30);
        $no=1;
        $data['chunks_question'] =$chunks_question;
        for($i=0; $i<sizeof($chunks_question);$i++){
          $data['data_question'.$no] = $chunks_question[$i];
          $no++;
        }
        $data['data_choice'] = $this->choice->findAll();
        $data['menu'] = 'quiz';
        return view('quiz/v_quiz',$data);
    }

    function simpan_quiz(){
      $data['status'] = 'sukses';
      echo json_encode($data);
    }
}
