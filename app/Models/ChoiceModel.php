<?php

namespace App\Models;

use CodeIgniter\Model;

class ChoiceModel extends Model
{
    // ...
    protected $DBGroup = 'default';
    protected $table      = 'choice';
    protected $primaryKey = 'id_choice';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['choice', 'value'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
