<?php

namespace App\Models;

use CodeIgniter\Model;

class QuestionModel extends Model
{
    // ...
    protected $DBGroup = 'default';
    protected $table      = 'question';
    protected $primaryKey = 'id_question';

    protected $useAutoIncrement = true;

    protected $returnType     = 'object';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['question', 'reverse'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
